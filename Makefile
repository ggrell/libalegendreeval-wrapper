#############################################################################################
#
# Makefile
#
# Copyright Gilbert Grell 2020
#
#  This program is free software: you can redistribute it and/or modify it under the terms
#  of the GNU General Public License as published by the Free Software Foundation, either
#  version 3 of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################################
#
# MAKEFILE
#
# This makefile is an addition to James C. Bremers Library
# for evaluation of associated Legendre functions
#
# The Library has been published in
# J. Comp. Phys 360, 15, 2018: https://doi.org/10.1016/j.jcp.2018.01.014
#
# The source code is available at github
# https://github.com/JamesCBremerJr/ALegendreEval
#
# The original License is GPL v.3.0
#
# This Makefile serves the purpose to compile the code into a
# shared object library that can be used further
#
# Its primary target is the Auger-SCI code that is currently under development
#
#
# Author: Gilbert Grell, IMDEA nanociencia, Madrid, 2020
####################################################################################
#
# usage: with the compiler suite for the Auger compilation loaded, execute make in this directory
#
FF = $(FC)
#
# flags for compilation of the object files
# position independent coed is needed, since the library might be loaded into arbitrary memory postion
FFLAGS = -O2 -fPIC
#FFLAGS = -O0 -fPIC
#
# flags for compilation of the test case with the shared library object
LDFLAGS = -Wl,-rpath,$(PWD)/$(installdir) -L$(PWD)/$(installdir)  -lalegendre
##############
# build directory:
builddir = build
##############
# include directory (.mod files)
incdir = inc
##############
# install directory
installdir = lib
##############
# code files
cde =  src/main/bessel_eval.f90 src/main/alegendre_eval.f90
obje =  $(builddir)/bessel_eval.o $(builddir)/alegendre_eval.o
#
libcde = src/library/libalegendre.f90
#
testcde = src/test/alegendre_test0.f90
#
newtestcde = src/newtest/alegendre_test.f90
##############
##############
#
# target library name
lib = libalegendre.so
##############
#
.PHONY : lib
lib : builddir installdir incdir  $(builddir)/$(lib) $(obje)
#
# compilation block
#
# create the shared library
# compile it directly, do not creat objects
#
# library
$(builddir)/$(lib): $(libcde) $(obje)
	$(FF) -shared $(FFLAGS) -o $@ $^ -J $(incdir)
#
# codefiles in main
$(builddir)/%.o: src/main/%.f90
	$(FF) $(FFLAGS) -c $< -o $@ -J $(incdir)
#
# create the build and binary directories
builddir:
	mkdir -p $(builddir)
installdir:
	mkdir -p $(installdir)
incdir:
	mkdir -p $(incdir)

.PHONY : test
test: $(builddir)/alegendre_test.exe

$(builddir)/alegendre_test.exe: $(testcde)
	$(FF)  -o $@ $^ -I$(incdir) $(LDFLAGS)

.PHONY : newtest
newtest: $(builddir)/alegendre_newtest.exe
$(builddir)/alegendre_newtest.exe: $(newtestcde)
	$(FF)  -o $@ $^ $(LDFLAGS)

.PHONY : install
install: installdir $(builddir)/$(lib)
	cp $(builddir)/$(lib) $(installdir)/$(lib)

.PHONY : clean
clean:
	rm -rf $(builddir) ; rm -rf $(installdir) ; rm -rf $(incdir)
