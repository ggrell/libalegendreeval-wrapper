# LibALegendreEval-wrapper #

This is a fork of the associated legendre polynomial library written by 
James C. Bremer.
The purpose of this fork is to supply a set of wrapper routines that allow the
Fortran modules of the original library to be accessed from a shared object 
library.
This is necessary to link to this library, when it should not be incorporated 
into the source code or executable of the calling program.

This has been mainly designed as an interface for another code that my coworkers 
and me are currently developing and preparing for publication

## Original source code ##

The original library has been published in 
[J. Comp. Phys 360, 15, 2018](https://doi.org/10.1016/j.jcp.2018.01.014)
and the source code is available under the GPLv3.0 license at 
[github](https://github.com/JamesCBremerJr/ALegendreEval) .

## Contributions: ##

*  The core of the Library has been written by J. C. Bremers and was just forked
   herein from his 
   [github repository](https://github.com/JamesCBremerJr/ALegendreEval)

*  The wrapper routines in `libalegendre.f90`, the `Makefile`, and the 
   modifications to the test routine in `src/alegendre_test.f90`, as well as the
   introduction of the `ALROOT` environmental variable in 
   `src/alegendre_eval.f90` and `src/bessel_eval.f90` have been contributed by 
   Gilbert Grell, IMDEA nanociencia Madrid, 2020

   
## License ##

This Library is redistributed under the GPLv3.0 license as the original code by 
J. C. Bremer.

## Installation and use with Auger-SCI ##

*  **This library** should be installed with the same compiler suite as the Auger-SCI 
   code.
*  There is no automated setup, so you will have to adapt the makefile.
   *  prerequisites: gfortran-based mpifort compile command
   *  `cd` to the source dir of the library
   *  `make && make install` : install the library
      *  libalegendre.so is in the lib dir.
*  The root directory of this repo is referred to as `$ALROOT`.
*  The library is found at `$ALROOT/lib/libalegendre.so`.
*  The binary data files are found at `$ALROOT/data`.
*  To use the library, the `$ALROOT` variable needs to be defined.
   *  e.g. set `export ALROOT=/path/to/root/dir` before using it
*  Testing:
   *  The test can be compiled with `make newtest`.
   *  from the root dir: execute the test as `./build/alegendre_newtest.exe`
*  **Using with Auger-SCI**: 
   *  The unrestricted-lmax branch provides the right interfaces.
   *  The `$ALROOT` variable needs to be defined prior to installation and 
      running the Auger-SCI code.
