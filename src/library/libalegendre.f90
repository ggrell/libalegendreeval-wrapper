!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! libalegendre.f90:
!
! Copyright: Gilbert Grell, 2020
!
!  This program is free software: you can redistribute it and/or modify it under the terms
!  of the GNU General Public License as published by the Free Software Foundation, either
!  version 3 of the License, or (at your option) any later version.
!
!  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
!  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!  See the GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! This is a small wrapper codefile that provides the
! interfaces to the routines
!
! alegendre_eval()
! alegendre_eval_init()
! bessel_eval_init()
!
! from the ALegendreEval library by James C. Bremer.
!
! to be compiled into a shared object library.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

subroutine load_bess_leg_data(sizeBess, sizeLeg)
  ! This subroutine is the library function to load the expansion coefficients for
  ! Bessel and associated Legendre polynomials into the RAM
  use :: besseleval, only: bessel_eval_init
  use :: alegendreeval, only : alegendre_eval_init
  implicit none
  !
  real(kind=8), intent(out) :: sizeBess
  ! estimate for the memory takeup of the bessel function expansion coefficients
  real(kind=8), intent(out) :: sizeLeg
  ! estimate for the memory takeup of the associated LEgendre function expansion coefficients
  !
  ! initialize the Bessel function storage:
  call bessel_eval_init(sizeBess)
  ! initialize the associated Legendre function storage:
  call alegendre_eval_init(sizeLeg)
end subroutine



subroutine alegendre_eval_wrap(dnu,dmu,t,alpha,alphader,vallogp,vallogq,valp,valq)
  ! Wrapper routine for a_legendre__eval that is included in the library
  use :: alegendreeval, only : alegendre_eval
  implicit double precision (a-h,o-z)
  !
  call alegendre_eval(dnu,dmu,t,alpha,alphader,vallogp,vallogq,valp,valq)
end subroutine
