!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! alegendre_test.f90
!
! Copyright Gilbert Grell, James C. Bremer, 2020
!
!  This program is free software: you can redistribute it and/or modify it under the terms
!  of the GNU General Public License as published by the Free Software Foundation, either
!  version 3 of the License, or (at your option) any later version.
!
!  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
!  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!  See the GNU General Public License for more details.
!
!  You should have received a copy of the GNU General Public License along with
!  this program.  If not, see <http://www.gnu.org/licenses/>.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Test suite for the wrapper library libalegendre.so, corresponding to libalegendre.f90
!
! adapted from J.ames C. Bremers alegendre_test0.f90.
!
! The sole purpose is to wrap the library by J.C. Bremer such that it can be
! accessed as a shared objuect file, without the need to incorporate it into
! a programs source.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

program alegendre_test
implicit double precision (a-h,o-z)

interface
  subroutine load_bess_leg_data(sizeBess, sizeLeg)
    ! This subroutine is the library function to load the expansion coefficients for
    ! Bessel and associated Legendre polynomials into the RAM
    implicit none
    !
    real(kind=8), intent(out) :: sizeBess
    ! estimate for the memory takeup of the bessel function expansion coefficients
    real(kind=8), intent(out) :: sizeLeg
  end subroutine
end interface

interface
  subroutine alegendre_eval_wrap(dnu,dmu,t,alpha,alphader,vallogp,vallogq,valp,valq)
    ! Wrapper routine for a_legendre__eval that is included in the library
    implicit double precision (a-h,o-z)
  end subroutine alegendre_eval_wrap
end interface


!call bessel_eval_init(dsize1)
!call alegendre_eval_init(dsize2)
call load_bess_leg_data(dsize1, dsize2)

pi = acos(-1.0d0)

print *,"in alegendre_test, loaded ",dsize1," megabytes of data for Bessel functions"
print *,"in alegendre_test, loaded ",dsize2," megabytes of data for ALFs"
print *,""


print *,"Enter real-valued nu betweeen 0 and 1,000,000"
read *,dnu
print *,"Enter real-valued mu between 0 and mu"
read *,dmu
print *,"Enter 0 < t < pi"
read *,t


!call alegendre_eval(dnu,dmu,t,alpha,alphader,vallogp,vallogq,valp,valq)
call alegendre_eval_wrap(dnu,dmu,t,alpha,alphader,vallogp,vallogq,valp,valq)
print *,""


if (alpha .ne. 0.0d0) then
print *,"phase function                          = ",alpha
print *,"phase function derivative               = ",alphader
else
print *,"log of P_nu^{-mu}(cos(t))sqrt(sin(t))   = ",vallogp
print *,"log of Q_nu^{-mu}(cos(t))sqrt(sin(t))   = ",vallogq
endif
print *,"value of P_nu^{-mu}(cos(t))sqrt(sin(t)) = ",valp
print *,"value of Q_nu^{-mu}(cos(t))sqrt(sin(t)) = ",valq


end program
